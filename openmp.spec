
%global libomp_version 12.0.1
%global libomp_srcdir openmp-%{libomp_version}%{?rc_ver:rc%{rc_ver}}.src

%ifarch ppc64le
%global libomp_arch ppc64
%else
%global libomp_arch %{_arch}
%endif

Name: libomp
Version: %{libomp_version}
Release: 1%{?dist}
Summary: OpenMP runtime for clang

License: NCSA
URL: http://openmp.llvm.org
Source0: https://github.com/llvm/llvm-project/releases/download/llvmorg-%{version}%{?rc_ver:-rc%{rc_ver}}/%{libomp_srcdir}.tar.xz

BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: cmake
BuildRequires: ninja-build
BuildRequires: elfutils-libelf-devel
BuildRequires: perl
BuildRequires: perl-Data-Dumper
BuildRequires: perl-Encode
BuildRequires: libffi-devel
 
# For gpg source verification
BuildRequires:	gnupg2
 
Requires: elfutils-libelf%{?isa}
 
# libomp does not support s390x.
ExcludeArch: s390x
 
%description
OpenMP runtime for clang.
 
%package devel
Summary: OpenMP header files
Requires: clang-resource-filesystem%{?isa} = %{version}
 
%description devel
OpenMP header files.
 
%package test
Summary: OpenMP regression tests
Requires: %{name}%{?isa} = %{version}-%{release}
Requires: %{name}-devel%{?isa} = %{version}-%{release}
Requires: clang
Requires: llvm
Requires: gcc
Requires: gcc-c++
Requires: python3-lit
 
%description test
OpenMP regression tests
 
%prep
%autosetup -n %{libomp_srcdir} -p2
 
%build
mkdir -p _build
cd _build
 
%cmake .. \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
	-DLIBOMP_INSTALL_ALIASES=OFF \
%if 0%{?__isa_bits} == 64
	-DOPENMP_LIBDIR_SUFFIX=64 \
%else
	-DOPENMP_LIBDIR_SUFFIX= \
%endif
 
%make_build
 
%install
cd _build
%make_install
# Test package setup
%global libomp_srcdir %{_datadir}/libomp/src/
%global libomp_testdir %{libomp_srcdir}/runtime/test/
%global lit_cfg %{libomp_testdir}/%{_arch}.site.cfg.py
%global lit_fedora_cfg %{_datadir}/libomp/lit.fedora.cfg.py
 
install -d %{buildroot}%{libomp_srcdir}/runtime
cp -R runtime/test  %{buildroot}%{libomp_srcdir}/runtime
cp -R runtime/src  %{buildroot}%{libomp_srcdir}/runtime

 
# Install test script
install -d %{buildroot}%{_libexecdir}/tests/libomp
 
# Remove static libraries with equivalent shared libraries
rm -rf %{buildroot}%{_libdir}/libarcher_static.a
 
%files
%license LICENSE.txt
%{_libdir}/libomp.so
%ifnarch %{arm}
%{_libdir}/libarcher.so
%endif
 
%files devel
%{_libdir}/../include/omp.h
%ifnarch %{arm}
%{_libdir}/../include/omp-tools.h
%{_libdir}/../include/ompt.h
# FIXME: This is probably wrong.  Seems like LIBOMP_HEADERS_INSTALL may
# not be respected.
%{_includedir}/ompt-multiplex.h
%endif
 
%files test
%{_datadir}/libomp
%{_libexecdir}/tests/libomp/
 
%changelog
* Thu July 27 2023 zhangwuji1995 <zhangwuji1@huawei.com> 12.0.1-1
- Init openmp 12.0.1
